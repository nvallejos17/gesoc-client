import { SET_CATEGORIES, GET_CATEGORIES, CATEGORIES_ERROR } from '../types'
import axios from 'axios'

export const setCategories = (categories) => (dispatch) => {
  dispatch({
    type: SET_CATEGORIES,
    payload: categories,
  })
}

export const getCategories = () => async (dispatch) => {
  try {
    const res = await axios.get('/api/categorias')

    dispatch({
      type: GET_CATEGORIES,
      payload: res.data.data,
    })
  } catch (err) {
    dispatch({
      type: CATEGORIES_ERROR,
    })
  }
}
