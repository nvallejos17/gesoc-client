import {
  SET_OUTCOMES,
  GET_OUTCOMES,
  ADD_OUTCOME,
  OUTCOMES_ERROR,
} from '../types'
import axios from 'axios'

// Actions
import { setAlert } from './alertsActions'

export const setOutcomes = (outcomes) => (dispatch) => {
  dispatch({
    type: SET_OUTCOMES,
    payload: outcomes,
  })
}

export const getOutcomes = () => async (dispatch) => {
  try {
    const res = await axios.get('/api/egresos')

    dispatch({
      type: GET_OUTCOMES,
      payload: res.data.data,
    })
  } catch (err) {
    dispatch({
      type: OUTCOMES_ERROR,
    })
  }
}

export const addOutcome = (outcome) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }

  const body = JSON.stringify({ body: outcome })
  try {
    const res = await axios.post('/api/egresos', body, config)

    dispatch({
      type: ADD_OUTCOME,
      payload: res.data.data,
    })

    dispatch(setAlert('Nuevo egreso agregado.', 'success'))
  } catch (err) {
    dispatch({
      type: OUTCOMES_ERROR,
    })

    dispatch(setAlert('Verifique los datos ingresados.', 'danger'))
  }
}
