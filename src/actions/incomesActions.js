import { SET_INCOMES, GET_INCOMES, ADD_INCOME, INCOMES_ERROR } from '../types'
import axios from 'axios'

// Actions
import { setAlert } from './alertsActions'

export const setIncomes = (incomes) => (dispatch) => {
  dispatch({
    type: SET_INCOMES,
    payload: incomes,
  })
}

export const getIncomes = () => async (dispatch) => {
  try {
    const res = await axios.get('/api/ingresos')

    dispatch({
      type: GET_INCOMES,
      payload: res.data.data,
    })
  } catch (err) {
    dispatch({
      type: INCOMES_ERROR,
    })
  }
}

export const addIncome = (income) => async (dispatch) => {
  // POST income to API
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }

  const body = JSON.stringify({ body: income })

  try {
    const res = await axios.post('/api/ingresos', body, config)

    dispatch({
      type: ADD_INCOME,
      payload: res.data.data,
    })

    dispatch(setAlert('Nuevo ingreso agregado.', 'success'))
  } catch (err) {
    dispatch({
      type: INCOMES_ERROR,
    })
    dispatch(setAlert('Verifique los datos ingresados.', 'danger'))
  }
}
