import {
  SET_LOADING,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
} from '../types'
import axios from 'axios'
import setAuthToken from '../utils/setAuthToken'

// Actions
import { setAlert } from './alertsActions'

export const setLoading = () => async (dispatch) => {
  dispatch({
    type: SET_LOADING,
  })
}

// Load user
export const loadUser = () => async (dispatch) => {
  if (localStorage.token) {
    setAuthToken(localStorage.token)
  }

  try {
    const res = await axios.get('/user')

    dispatch({
      type: USER_LOADED,
      payload: res.data,
    })
  } catch (err) {
    dispatch({
      type: AUTH_ERROR,
    })
  }
}

// Register user
export const register = (registerData) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }

  const body = JSON.stringify(registerData)

  try {
    const res = await axios.post('/register', body, config)

    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data,
    })

    if (res.data.respuesta === 'Usuario creado exitosamente') {
      dispatch(setAlert(res.data.respuesta, 'success'))
    } else {
      dispatch(setAlert(res.data.respuesta, 'danger'))
    }
  } catch (err) {
    dispatch({
      type: REGISTER_FAIL,
    })

    dispatch(
      setAlert(
        'Algo salió mal. Comprueba los datos e intenta nuevamente: ' +
          err.message,
        'danger'
      )
    )
  }
}

// Login user
export const login = (loginData) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  }

  const body = JSON.stringify(loginData)

  try {
    dispatch(setLoading())

    const res = await axios.post('/login', body, config)

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    })

    dispatch(loadUser())
    dispatch(setAlert('Bienvenido.', 'success'))
  } catch (err) {
    dispatch({
      type: LOGIN_FAIL,
    })

    dispatch(
      setAlert(
        'Algo salió mal. Comprueba tus credenciales e intenta nuevamente: ' +
          err.message,
        'danger'
      )
    )
  }
}

export const logout = () => (dispatch) => {
  dispatch({ type: LOGOUT })
}
