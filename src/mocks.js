import { v4 as uuidv4 } from 'uuid'

export const userMock = {
  id: 1,
  nombreUsuario: 'jdoe',
  isAdmin: false,
  mensajes: [
    {
      id: 1,
      contenido: 'Contenido del mensaje',
      fecha: '11-11-2020',
      leido: true,
    },
    {
      id: 2,
      contenido: 'Contenido del mensaje',
      fecha: '11-11-2020',
      leido: false,
    },
  ],
  organizacionJuridica: {
    id: 1,
    codigoIGJ: 1111,
    razonSocial: 'Carrefour SA',
    nombreFicticio: 'Carrefour',
    cuit: '11-111111-1',
    actividad: 'OTRA',
    activo: 1000000,
    cantidadPersonal: 100,
    estructuraJuridica: 'MEDIANA1',
    promedioVentasAnuales: 100000,
    direccionPostal: {
      calle: 'Calle falsa',
      altura: '111',
      piso: '1',
      departamento: 'A',
      codigoPostal: '1111',
      ciudad: {
        id: 1,
        nombre: 'Quilmes',
        provincia: 'Buenos Aires',
        pais: 'Argentina',
      },
    },
    organizacionesBase: [
      {
        id: 1,
        descripcion: 'Supermercado abierto las 24 horas',
        nombreFicticio: 'Carrefour Express',
      },
    ],
    criterios: [],
    categorias: [],
    ingresos: [
      {
        id: 1,
        fecha: '11-11-2020',
        descripcion: 'Venta de carne',
        montoTotal: 1000.0,
      },
      {
        id: 2,
        fecha: '11-11-2020',
        descripcion: 'Venta de legumbres',
        montoTotal: 1000.0,
      },
    ],
    egresos: [
      {
        id: 1,
        fecha: '11-11-2020',
        detalle: [
          {
            id: 1,
            descripcion: 'Papel film',
            valor: 100.0,
          },
          {
            id: 2,
            descripcion: 'Cajas de 40x40x60',
            valor: 200.0,
          },
          {
            id: 3,
            descripcion: 'Cinta de embalar',
            valor: 300.0,
          },
        ],
        medioDePago: {
          id: 1,
          tipo: 'CREDIT_CARD',
          numero: 1111111111111111,
        },
        proveedor: {
          id: 1,
          razonSocial: 'La papelera SA',
          numeroDNI: 11111111,
          direccionPostal: {
            calle: 'Calle de la papelera',
            altura: '111',
            piso: '1',
            departamento: 'A',
            codigoPostal: '2222',
            ciudad: {
              id: 2,
              nombre: 'Lomas de Zamora',
              provincia: 'Buenos Aires',
              pais: 'Argentina',
            },
          },
        },
        valorTotal: 600.0,
        presupuestosRequeridos: 2,
        operacionValida: true,
        categorias: [],
        presupuestos: [
          {
            id: 1,
            detalle: [
              {
                id: 4,
                descripcion: 'Papel film',
                valor: 100.0,
              },
              {
                id: 5,
                descripcion: 'Cajas de 40x40x60',
                valor: 200.0,
              },
              {
                id: 6,
                descripcion: 'Cinta de embalar',
                valor: 300.0,
              },
            ],
            valorTotal: 600.0,
          },
          {
            id: 2,
            detalle: [
              {
                id: 7,
                descripcion: 'Papel film',
                valor: 110.0,
              },
              {
                id: 8,
                descripcion: 'Cajas de 40x40x60',
                valor: 220.0,
              },
              {
                id: 9,
                descripcion: 'Cinta de embalar',
                valor: 330.0,
              },
            ],
            valorTotal: 660.0,
          },
        ],
        presupuestoElegido: 1,
        revisores: [1, 2, 4, 9],
      },
      {
        id: 2,
        fecha: '12-17-2020',
        detalle: [
          {
            id: 10,
            descripcion: 'Notebook Dell i5 4GB',
            valor: 90000.0,
          },
          {
            id: 11,
            descripcion: 'Monitor Dell 4K',
            valor: 50000.0,
          },
        ],
        medioDePago: {
          id: 2,
          tipo: 'DEBIT_CARD',
          numero: 222222222222,
        },
        proveedor: {
          id: 2,
          razonSocial: 'Dell SA',
          numeroDNI: 22222222,
          direccionPostal: {
            calle: 'Calle de Dell',
            altura: '222',
            piso: '',
            departamento: 'B',
            codigoPostal: '1200',
            ciudad: {
              id: 20,
              nombre: 'Ciudad Autonoma de Buenos Aires',
              provincia: 'Capital Federal',
              pais: 'Argentina',
            },
          },
        },
        valorTotal: 140000.0,
        presupuestosRequeridos: 0,
        operacionValida: true,
        categorias: [],
        presupuestos: [],
        presupuestoElegido: null,
        revisores: [1, 9],
      },
    ],
  },
}

export const incomesMock = [
  { id: uuidv4(), desc: 'Descripción 1', amount: 100, date: '10/10/2020' },
]

export const outcomesMock = [
  {
    id: uuidv4(),
    items: [
      { id: uuidv4(), desc: 'Descripción del ítem', itemPrice: 100 },
      { id: uuidv4(), desc: 'Descripción del ítem', itemPrice: 200 },
    ],
    date: '10/10/2020',
    budgetQty: 0,
    category: 'Categoría',
    criteria: 'Criterio',
  },
]

export const criteriasMock = [
  { id: uuidv4(), desc: 'Criterio 1' },
  { id: uuidv4(), desc: 'Criterio 2' },
  { id: uuidv4(), desc: 'Criterio 3' },
]

export const categoriesMock = [
  { id: uuidv4(), desc: 'Categoría 1' },
  { id: uuidv4(), desc: 'Categoría 2' },
  { id: uuidv4(), desc: 'Categoría 3' },
]

export const msgsMock = [
  { id: uuidv4(), msg: 'Mensaje 1', date: '10/10/2020', read: false },
  { id: uuidv4(), msg: 'Mensaje 2', date: '10/20/2020', read: true },
  { id: uuidv4(), msg: 'Mensaje 3', date: '10/30/2020', read: false },
]
