import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import setAuthToken from './utils/setAuthToken'

import Navbar from './components/Navbar'
import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register'
import Dashboard from './components/Dashboard'
import Mailbox from './components/Mailbox'
import NewIncome from './components/incomes/NewIncome'
import NewOutcome from './components/outcomes/NewOutcome'
import PrivateRoute from './components/routing/PrivateRoute'
import Alerts from './components/Alerts'

// Redux
import { Provider } from 'react-redux'
import store from './store'

// Actions
import { loadUser } from './actions/authActions'

if (localStorage.token) {
  setAuthToken(localStorage.token)
}

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser())
  }, [])

  return (
    <Provider store={store}>
      <Router>
        <Navbar />
        <Alerts />
        <div className='container'>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/login' component={Login} />
            <PrivateRoute exact path='/register' component={Register} />
            <PrivateRoute exact path='/dashboard' component={Dashboard} />
            <PrivateRoute exact path='/mailbox' component={Mailbox} />
            <PrivateRoute exact path='/new-income' component={NewIncome} />
            <PrivateRoute exact path='/new-outcome' component={NewOutcome} />
          </Switch>
        </div>
      </Router>
    </Provider>
  )
}

export default App
