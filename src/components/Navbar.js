import React from 'react'
import { Link } from 'react-router-dom'

// Redux
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

// Actions
import { logout } from '../actions/authActions'

const Navbar = ({ auth: { isAuthenticated, loading, user }, logout }) => {
  const authLinks = (
    <>
      {user && user.isAdmin && (
        <Link className='nav-link' to='/register'>
          Registrar usuario
        </Link>
      )}
      <Link className='nav-link' to='/dashboard'>
        Dashboard
      </Link>
      <Link className='nav-link' to='/mailbox'>
        Mensajes
      </Link>
      <Link className='nav-link' onClick={logout}>
        <i className='fas fa-sign-out-alt'></i> Salir
      </Link>
    </>
  )

  const guestLinks = (
    <>
      <Link className='nav-link' to='/login'>
        Ingresar
      </Link>
      {/* <Link className='nav-link' to='/register'>
        Registrarse
      </Link> */}
    </>
  )

  return (
    <nav className='navbar sticky-top navbar-expand-lg navbar-light bg-light'>
      <Link className='navbar-brand' to='/'>
        GeSoc
      </Link>

      {/* Toggler */}
      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#mainNavLinks'
        aria-controls='mainNavLinks'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>

      {/* Links */}
      <div className='collapse navbar-collapse' id='mainNavLinks'>
        <div className='navbar-nav ml-auto text-center'>
          {!loading && isAuthenticated ? authLinks : guestLinks}
        </div>
      </div>
    </nav>
  )
}

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
})

export default connect(mapStateToProps, { logout })(Navbar)
