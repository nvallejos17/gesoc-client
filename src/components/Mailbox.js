import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

// Components
import MsgItem from './atoms/MsgItem'

const MailboxWrapper = styled.div`
  height: calc(100vh - 91px);
`

const Mailbox = ({ msgs }) => {
  return (
    <MailboxWrapper>
      <div className='h-100 d-flex flex-column py-3'>
        <h3 className='card-title'>Bandeja de mensajes</h3>
        <div className='h-100 overflow-auto'>
          {msgs && msgs.length ? (
            msgs.map((item) => <MsgItem key={item.id} message={item}></MsgItem>)
          ) : (
            <p>No hay mensajes.</p>
          )}
        </div>
      </div>
    </MailboxWrapper>
  )
}

Mailbox.propTypes = {
  msgs: PropTypes.array.isRequired,
}

export default Mailbox
