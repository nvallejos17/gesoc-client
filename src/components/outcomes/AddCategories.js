import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

// Redux
import { connect } from 'react-redux'

// Actions
import { getCategories } from '../../actions/categoriesActions'

const CategoryItem = ({
  id,
  descripcion,
  addCategory,
  removeCategory,
  selected,
}) => {
  return (
    <button
      type='button'
      className={`btn btn-${selected ? 'danger' : 'outline-success'} m-1`}
      onClick={() => (selected ? removeCategory(id) : addCategory(id))}
    >
      {descripcion}
      <i className={`fas fa-${selected ? 'minus' : 'plus'} ml-2`}></i>
    </button>
  )
}

const AddCategories = ({
  outcomeCategories,
  addCategory,
  removeCategory,
  categories,
  getCategories,
  auth: { user },
}) => {
  useEffect(() => {
    getCategories()
    // eslint-disable-next-line
  }, [user])

  return (
    <div className='border mb-3 p-3'>
      <h3 className='card-title'>Categorías</h3>
      <div className='d-flex flex-wrap'>
        {categories && categories.length ? (
          categories.map(({ id, descripcion }) => (
            <CategoryItem
              id={id}
              descripcion={descripcion}
              addCategory={addCategory}
              removeCategory={removeCategory}
              selected={outcomeCategories.includes(id)}
            />
          ))
        ) : (
          <div>No hay categorías.</div>
        )}
      </div>
    </div>
  )
}

AddCategories.propTypes = {
  categories: PropTypes.array.isRequired,
  getCategories: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  categories: state.categories,
})

export default connect(mapStateToProps, { getCategories })(AddCategories)
