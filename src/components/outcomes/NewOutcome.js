import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'

// Redux
import { connect } from 'react-redux'

// Actions
import { addOutcome } from '../../actions/outcomesActions'

// Components
import AddBudgets from './AddBudgets'
import AddCategories from './AddCategories'
import AddItems from './AddItems'

const NewOutcomeWrapper = styled.div`
  height: calc(100vh - 91px);
`

const NewOutcome = ({ addOutcome }) => {
  const [step, setStep] = useState(0)

  const [outcomeData, setOutcomeData] = useState({
    items: [],
    budgetsQty: 0,
    budgets: [],
    paymentMethodType: '',
    paymentMethodNumber: 0,
    providerName: '',
    providerNumber: 0,
    categories: [],
  })

  const onChange = (e) =>
    setOutcomeData({
      ...outcomeData,
      [e.target.name]: e.target.value,
    })

  const addItem = (item) =>
    setOutcomeData({
      ...outcomeData,
      items: [...outcomeData.items, item],
    })

  const removeItem = (item) => {
    setOutcomeData({
      ...outcomeData,
      items: [...outcomeData.items.filter((i) => i !== item)],
    })
  }

  const addBudget = (budget) => {
    setOutcomeData({
      ...outcomeData,
      budgets: [...outcomeData.budgets, budget],
    })
  }

  const removeBudget = (budget) => {
    setOutcomeData({
      ...outcomeData,
      budgets: [...outcomeData.budgets.filter((b) => b !== budget)],
    })
  }

  const addCategory = (categoryId) => {
    setOutcomeData({
      ...outcomeData,
      categories: Array.from(
        new Set([...outcomeData.categories, categoryId])
      ).sort(),
    })
  }

  const removeCategory = (categoryId) => {
    setOutcomeData({
      ...outcomeData,
      categories: [...outcomeData.categories.filter((c) => c !== categoryId)],
    })
  }

  const nextStep = (step) => setStep(step)

  const confirmOutcome = (e) => {
    e.preventDefault()
    let preparedData = {
      fecha: moment().format('MMM DD, yyyy h:mm:ss a'),
      detalle: outcomeData.items.map((i) => {
        return { descripcion: i.desc, valorItem: i.itemValue }
      }),
      medioDePago: {
        tipo: outcomeData.paymentMethodType,
        numero: outcomeData.paymentMethodNumber,
      },
      proveedor: {
        razonSocial: outcomeData.providerName,
        numeroDNI: parseInt(outcomeData.providerNumber),
      },
      presupuestosRequeridos: parseInt(outcomeData.budgetsQty),
      categorias: outcomeData.categories,
      presupuestos: outcomeData.budgets.map((b) => {
        return {
          detalle: b.items.map((i) => {
            return { descripcion: i.desc, valorItem: i.itemValue }
          }),
          valorTotal: b.totalValue,
        }
      }),
    }
    addOutcome(preparedData)
    setStep(0)
    setOutcomeData({
      items: [],
      budgetsQty: 0,
      budgets: [],
      paymentMethodType: '',
      paymentMethodNumber: 0,
      providerName: '',
      providerNumber: 0,
      categories: [],
    })
  }

  return (
    <NewOutcomeWrapper>
      <div className='h-100 py-3 d-flex flex-column'>
        <h3 className='card-title'>Nuevo egreso</h3>
        {step === 0 && (
          <AddItems
            items={outcomeData.items}
            addItem={addItem}
            removeItem={removeItem}
            nextStep={nextStep}
          />
        )}
        {step === 1 && (
          <>
            <div className='input-group mb-3'>
              <div className='input-group-prepend'>
                <span className='input-group-text'>
                  Cantidad de presupuestos
                </span>
              </div>
              <input
                type='number'
                min='0'
                step='1'
                className='form-control'
                name='budgetsQty'
                value={outcomeData.budgetsQty}
                onChange={onChange}
              />
            </div>
            <button
              className='btn btn-success'
              onClick={() =>
                outcomeData.budgetsQty === 0 ? nextStep(3) : nextStep(2)
              }
            >
              Siguiente
            </button>
          </>
        )}
        {step === 2 && (
          <AddBudgets
            items={outcomeData.items}
            budgetsQty={outcomeData.budgetsQty}
            budgets={outcomeData.budgets}
            addBudget={addBudget}
            removeBudget={removeBudget}
            nextStep={nextStep}
          />
        )}
        {step === 3 && (
          <form onSubmit={confirmOutcome}>
            <div className='d-flex'>
              <div className='w-50 border mr-2 mb-3 p-3'>
                <h3 className='card-title'>Medio de pago</h3>
                <div className='form-group'>
                  <label htmlFor='paymentMethodType'>
                    Tipo de medio de pago (CREDIT_CARD, DEBIT_CARD, TICKET, ATM,
                    ACCOUNT_MONEY)
                  </label>
                  <input
                    type='text'
                    className='form-control'
                    name='paymentMethodType'
                    value={outcomeData.paymentMethodType}
                    onChange={onChange}
                    required
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='paymentMethodNumber'>
                    Nº de medio de pago
                  </label>
                  <input
                    type='number'
                    className='form-control'
                    name='paymentMethodNumber'
                    value={outcomeData.paymentMethodNumber}
                    onChange={onChange}
                    required
                  />
                </div>
              </div>

              <div className='w-50 border ml-2 mb-3 p-3'>
                <h3 className='card-title'>Proveedor</h3>
                <div className='form-group'>
                  <label htmlFor='provider'>Nombre del proveedor</label>
                  <input
                    type='text'
                    className='form-control'
                    name='providerName'
                    value={outcomeData.providerName}
                    onChange={onChange}
                    required
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='password'>Nº de proveedor (DNI/CUIT)</label>
                  <input
                    type='number'
                    className='form-control'
                    name='providerNumber'
                    value={outcomeData.providerNumber}
                    onChange={onChange}
                    required
                  />
                </div>
              </div>
            </div>

            <AddCategories
              outcomeCategories={outcomeData.categories}
              addCategory={addCategory}
              removeCategory={removeCategory}
            />

            <button type='submit' className='btn btn-success w-100'>
              Confirmar
            </button>
          </form>
        )}
      </div>
    </NewOutcomeWrapper>
  )
}

NewOutcome.propTypes = {
  addOutcome: PropTypes.func.isRequired,
}

export default connect(null, { addOutcome })(NewOutcome)
