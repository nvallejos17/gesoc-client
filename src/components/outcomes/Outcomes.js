import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

// Redux
import { connect } from 'react-redux'

// Components
import OutcomeItem from '../atoms/OutcomeItem'

// Actions
import { getOutcomes } from '../../actions/outcomesActions'

const Outcomes = ({ outcomes, getOutcomes }) => {
  useEffect(() => {
    getOutcomes()
    // eslint-disable-next-line
  }, [])

  return outcomes && outcomes.length ? (
    outcomes.map((item) => <OutcomeItem key={item.id} outcome={item} />)
  ) : (
    <p>No hay egresos.</p>
  )
}

Outcomes.propTypes = {
  outcomes: PropTypes.array.isRequired,
  getOutcomes: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  outcomes: state.outcomes,
})

export default connect(mapStateToProps, { getOutcomes })(Outcomes)
