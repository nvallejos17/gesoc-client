import React, { useState } from 'react'
import styled from 'styled-components'

const EditBudgetItem = styled.div`
  width: 100vw;
  height: 100vh;

  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;

  display: flex;
  justify-content: center;
  align-items: center;

  background: rgba(0, 0, 0, 0.9);
`

const Budget = ({ items, budgets, budgetsQty, addBudget }) => {
  const [currentBudget, setCurrentBudget] = useState({
    items: items.slice(),
    totalValue: items
      .map((i) => parseFloat(i.itemValue))
      .reduce((a, b) => a + b),
  })

  const [currentItem, setCurrentItem] = useState(null)

  const selectCurrent = (id) =>
    setCurrentItem(currentBudget.items.find((i) => i.id === id))

  const onChange = (e) =>
    setCurrentItem({
      ...currentItem,
      itemValue: parseFloat(e.target.value),
    })

  const confirmItem = (e) => {
    e.preventDefault()
    let itemsCopy = currentBudget.items
    let itemIndex = itemsCopy.findIndex((i) => i.id === currentItem.id)
    itemsCopy[itemIndex] = currentItem
    setCurrentBudget({
      items: itemsCopy,
      totalValue: itemsCopy
        .map((i) => parseFloat(i.itemValue))
        .reduce((a, b) => a + b),
    })
    setCurrentItem(null)
  }

  const confirmBudget = (e) => {
    e.preventDefault()
    addBudget({
      ...currentBudget,
      items: currentBudget.items.map((i) => {
        return { desc: i.desc, itemValue: i.itemValue }
      }),
    })
    setCurrentBudget({
      items: items.slice(),
      totalValue: items
        .map((i) => parseFloat(i.itemValue))
        .reduce((a, b) => a + b),
    })
    setCurrentItem(null)
  }

  return (
    <>
      {currentItem && (
        <EditBudgetItem>
          <div className='d-flex flex-column bg-light rounded p-3'>
            <div className='input-group mb-3'>
              <div className='input-group-prepend'>
                <span className='input-group-text mr-2'>
                  {currentItem.desc}
                </span>
                <span className='input-group-text'>$</span>
              </div>
              <input
                type='number'
                min='0'
                step='any'
                className='form-control'
                value={currentItem.itemValue}
                onChange={onChange}
              />
            </div>
            <button className='btn btn-success' onClick={confirmItem}>
              Confirmar precio
            </button>
          </div>
        </EditBudgetItem>
      )}
      {budgets && budgets.length < parseInt(budgetsQty) && (
        <div className='border mb-3 p-3'>
          <h3 className='card-title'>
            Presupuesto {budgets.length + 1} de {parseInt(budgetsQty)}
          </h3>
          {currentBudget.items && currentBudget.items.length && (
            <table className='table table-sm table-striped table-borderless table-hover text-center'>
              <thead>
                <tr>
                  <th>Ítem Nº</th>
                  <th>Descripción</th>
                  <th>Precio</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {currentBudget.items.map((item, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.desc}</td>
                    <td>${item.itemValue}</td>
                    <td className='text-right'>
                      <button
                        className='btn btn-sm btn-warning'
                        onClick={() => selectCurrent(item.id)}
                      >
                        <i className='fas fa-edit'></i>
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
          <button className='btn btn-success w-100' onClick={confirmBudget}>
            Confirmar presupuesto
          </button>
        </div>
      )}
    </>
  )
}

const AddBudgets = ({
  items,
  budgetsQty,
  budgets,
  addBudget,
  removeBudget,
  nextStep,
}) => {
  return (
    <div>
      <Budget
        items={items}
        budgets={budgets}
        budgetsQty={budgetsQty}
        addBudget={addBudget}
      />

      <div className='border mb-3 p-3'>
        <h3 className='card-title'>Presupuestos agregados</h3>
        {budgets && budgets.length ? (
          <table className='table table-sm table-striped table-borderless table-hover text-center'>
            <thead>
              <tr>
                <th>Presupuesto Nº</th>
                <th>Valor total</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {budgets.map((budget, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>${budget.totalValue.toFixed(2)}</td>
                  <td className='text-right'>
                    <button
                      className='btn btn-sm btn-danger'
                      onClick={() => removeBudget(budget)}
                    >
                      <i className='fas fa-times'></i>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <p className='m-0 text-center'>No se agregaron presupuestos.</p>
        )}
      </div>

      <div className='border p-3'>
        {budgets && budgets.length < budgetsQty ? (
          <p className='mb-0'>
            Faltan agregar {budgetsQty - budgets.length} presupuestos.
          </p>
        ) : (
          <>
            <p className='text-center'>
              Se agregaron todos los presupuestos requeridos.
            </p>
            <button
              className='btn btn-success w-100'
              onClick={() => nextStep(3)}
            >
              Siguiente
            </button>
          </>
        )}
      </div>
    </div>
  )
}

export default AddBudgets
