import React, { useState } from 'react'
import { v4 as uuid } from 'uuid'

const AddItems = ({ items, addItem, removeItem, nextStep }) => {
  const [item, setItem] = useState({
    id: uuid(),
    desc: '',
    itemValue: 0,
  })

  const onChange = (e) =>
    setItem({
      ...item,
      [e.target.name]: e.target.value,
    })

  const onSubmit = (e) => {
    e.preventDefault()
    addItem({ ...item, itemValue: parseFloat(item.itemValue) })
    setItem({
      id: uuid(),
      desc: '',
      itemValue: 0,
    })
  }

  return (
    <div className='border d-flex flex-column mb-3 p-3'>
      <form className='mb-3 ml-auto' onSubmit={onSubmit}>
        <div className='form-row align-items-center'>
          <div className='col-auto'>
            <input
              type='text'
              className='form-control mr-2'
              name='desc'
              placeholder='Descripción'
              value={item.desc}
              onChange={onChange}
              required
            />
          </div>
          <div className='col-auto'>
            <div className='input-group'>
              <div className='input-group-prepend'>
                <span className='input-group-text'>$</span>
              </div>
              <input
                type='number'
                min='0'
                step='any'
                className='form-control mr-2'
                name='itemValue'
                value={item.itemValue}
                onChange={onChange}
              />
            </div>
          </div>
          <div className='col-auto'>
            <button type='submit' className='btn btn-success'>
              <i className='fas fa-plus'></i>
            </button>
          </div>
        </div>
      </form>

      {items && items.length ? (
        <>
          <table className='table table-sm table-striped table-borderless table-hover text-center'>
            <tbody>
              {items.map((item, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.desc}</td>
                  <td>${parseFloat(item.itemValue).toFixed(2)}</td>
                  <td className='text-right'>
                    <button
                      className='btn btn-sm btn-danger'
                      onClick={() => removeItem(item)}
                    >
                      <i className='fas fa-times'></i>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <button className='btn btn-success' onClick={() => nextStep(1)}>
            Siguiente
          </button>
        </>
      ) : (
        <p className='m-0 text-center'>No se agregaron ítems.</p>
      )}
    </div>
  )
}

export default AddItems
