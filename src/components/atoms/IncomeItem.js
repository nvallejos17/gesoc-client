import React from 'react'
import moment from 'moment'

const IncomeItem = ({ income }) => {
  const { descripcion, valorTotal, fecha } = income

  return (
    <div className='card mb-2'>
      <div className='card-body'>
        <div className='card-title'>{descripcion}</div>
        <p className='card-text'>
          Valor total:{' '}
          {valorTotal ? (
            `$${valorTotal.toFixed(2)}`
          ) : (
            <i className='fas fa-exclamation-triangle' />
          )}{' '}
          -{' '}
          <span className='text-muted'>
            Fecha: {moment(fecha).format('DD/MM/YYYY')}
          </span>
        </p>
        {/* <button className='btn btn-sm btn-primary'>Ver</button> */}
      </div>
    </div>
  )
}

export default IncomeItem
