import React, { useState } from 'react'
import styled from 'styled-components'
import moment from 'moment'

const ViewOutcomeWrapper = styled.div`
  width: 100vw;
  height: 100vh;

  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;

  display: flex;
  justify-content: center;
  align-items: center;

  background: rgba(0, 0, 0, 0.9);
`

const OutcomeItem = ({ outcome }) => {
  const { id, fecha, valorTotal, detalle } = outcome

  const [selected, setSelected] = useState(false)

  return (
    <>
      <div className='card mb-2'>
        <div className='card-body'>
          <div className='card-title'>Egreso #{id}</div>
          <p className='card-text'>
            Valor total:{' '}
            {valorTotal ? (
              `$${valorTotal.toFixed(2)}`
            ) : (
              <i className='fas fa-exclamation-triangle' />
            )}{' '}
            -{' '}
            <span className='text-muted'>
              Fecha: {moment(fecha).format('DD/MM/YYYY')}
            </span>
          </p>
          <button
            className='btn btn-sm btn-primary'
            onClick={() => {
              console.log('Show modal')
              setSelected(true)
            }}
          >
            Ver
          </button>
        </div>
      </div>

      {selected && (
        <ViewOutcomeWrapper>
          <div className='bg-light rounded p-3' style={{ minWidth: '720px' }}>
            <h3 className='card-title'>ID: {id}</h3>
            <p className='card-text text-muted'>
              Fecha: {moment(fecha).format('DD/MM/YYYY')}
            </p>
            <table className='table table-sm table-striped table-borderless table-hover text-center'>
              <thead>
                <tr>
                  <th>Item Nº</th>
                  <th>Descripción</th>
                  <th>Precio</th>
                </tr>
              </thead>
              <tbody>
                {detalle.map((item, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.descripcion}</td>
                    <td>
                      {item.valorItem ? (
                        `$${item.valorItem.toFixed(2)}`
                      ) : (
                        <i className='fas fa-exclamation-triangle' />
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <p className='card-text'>
              Monto total:{' '}
              {valorTotal ? (
                `$${valorTotal.toFixed(2)}`
              ) : (
                <i className='fas fa-exclamation-triangle' />
              )}
            </p>
            <button
              type='button'
              className='btn btn-outline-primary'
              onClick={() => setSelected(false)}
            >
              Cerrar
            </button>
          </div>
        </ViewOutcomeWrapper>
      )}
    </>
  )
}

export default OutcomeItem
