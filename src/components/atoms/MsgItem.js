import React, { useState } from 'react'
import moment from 'moment'
import styled, { css } from 'styled-components'

const MsgWrapper = styled.div(({ unread }) => {
  return css`
    margin-bottom: 0.5rem;

    border: 1px solid lightgrey;

    ${unread &&
    css`
      border-left: 3px solid black;
    `}
  `
})

const MsgTitle = styled.div(({ expanded }) => {
  return css`
    padding: 1rem;

    display: flex;
    justify-content: space-between;
    align-items: center;

    background: ${expanded ? '#fafafa' : 'none'};

    cursor: pointer;

    &:hover {
      background: #fafafa;
    }
  `
})

const MsgItem = ({ message }) => {
  const { msg, date, read } = message

  const [expanded, setExpanded] = useState(false)
  const [unread, setUnread] = useState(!read)

  const handleClick = () => {
    unread && setUnread(false)
    setExpanded(!expanded)
  }

  return (
    <MsgWrapper unread={unread}>
      <MsgTitle onClick={handleClick} expanded={expanded}>
        <div>
          <i
            className={`${
              unread ? 'fas fa-envelope' : 'far fa-envelope-open'
            } mr-3`}
          ></i>
          {moment(date).format('DD/MM/YYYY')}
        </div>
        <div>
          <i className={`fas fa-chevron-${expanded ? 'up' : 'down'}`}></i>
        </div>
      </MsgTitle>

      {expanded && <div className='p-3'>{msg}</div>}
    </MsgWrapper>
  )
}

export default MsgItem
