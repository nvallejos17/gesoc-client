import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

// Redux
import { connect } from 'react-redux'

// Components
import IncomeItem from '../atoms/IncomeItem'

// Actions
import { getIncomes } from '../../actions/incomesActions'

const Incomes = ({ incomes, getIncomes }) => {
  useEffect(() => {
    getIncomes()
    // eslint-disable-next-line
  }, [])

  return incomes && incomes.length ? (
    incomes.map((item) => <IncomeItem key={item.id} income={item} />)
  ) : (
    <p>No hay ingresos.</p>
  )
}

Incomes.propTypes = {
  incomes: PropTypes.array.isRequired,
  getIncomes: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  incomes: state.incomes,
})

export default connect(mapStateToProps, { getIncomes })(Incomes)
