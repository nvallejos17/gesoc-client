import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'

// Redux
import { connect } from 'react-redux'

// Actions
import { addIncome } from '../../actions/incomesActions'

const NewIncomeWrapper = styled.div`
  height: calc(100vh - 91px);
`

const NewIncome = ({ addIncome }) => {
  const [formData, setFormData] = useState({
    description: '',
    totalValue: 0,
  })

  const { description, totalValue } = formData

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value })

  const onSubmit = (e) => {
    e.preventDefault()
    addIncome({
      descripcion: description,
      valorTotal: parseFloat(totalValue),
      fecha: moment().format('MMM DD, yyyy h:mm:ss a'),
    })

    setFormData({
      description: '',
      totalValue: 0,
    })
  }

  return (
    <NewIncomeWrapper>
      <div className='h-100 py-3 d-flex flex-column'>
        <h3 className='card-title'>Nuevo ingreso</h3>
        <form onSubmit={onSubmit}>
          <div className='form-group'>
            <label htmlFor='username'>Descripción</label>
            <input
              type='text'
              className='form-control'
              name='description'
              value={description}
              onChange={onChange}
              required
            />
          </div>
          <div className='form-group'>
            <label htmlFor='password'>Monto total</label>
            <input
              type='number'
              className='form-control'
              name='totalValue'
              value={totalValue}
              onChange={onChange}
              required
            />
          </div>
          <button type='submit' className='btn btn-primary'>
            Crear operación
          </button>
        </form>
      </div>
    </NewIncomeWrapper>
  )
}

NewIncome.propTypes = {
  addIncome: PropTypes.func.isRequired,
}

export default connect(null, { addIncome })(NewIncome)
