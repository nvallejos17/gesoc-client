import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const HomeWrapper = styled.div`
  padding-top: 2rem;

  text-align: center;
`

const Home = () => {
  return (
    <HomeWrapper>
      <h3 className='mb-3'>¡Bienvenido a GeSoc!</h3>
      <Link className='btn btn-primary' to='/login'>
        Ingresar
      </Link>
      {/* <Link className='btn btn-primary' to='/register'>
        Registrarse
      </Link> */}
    </HomeWrapper>
  )
}

export default Home
