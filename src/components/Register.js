import React, { useState } from 'react'
import styled from 'styled-components'

// Redux
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Actions
import { register } from '../actions/authActions'

const RegisterWrapper = styled.div`
  max-width: 480px;
`

const Register = ({ register, auth: { loading, user } }) => {
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    password2: '',
  })

  const { username, password, password2 } = formData

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value })

  const onSubmit = (e) => {
    e.preventDefault()
    if (password !== password2) {
      window.alert('Las contraseñas no coinciden.')
    } else {
      register({
        user: username,
        pass: password,
        organizacion_id: user.organizacion.id,
      })
    }
  }

  return !loading && !user.isAdmin ? (
    <p className='text-center mt-3'>
      No cuentas con los permisos suficientes para registrar un usuario.
    </p>
  ) : (
    <RegisterWrapper className='m-auto py-3'>
      <div className='card'>
        <div className='card-body'>
          <h3 className='card-title text-center'>Registrar usuario</h3>
          <form onSubmit={onSubmit}>
            <div className='form-group'>
              <label htmlFor='username'>Usuario</label>
              <input
                type='text'
                className='form-control'
                name='username'
                value={username}
                onChange={onChange}
                required
              />
            </div>
            <div className='form-group'>
              <label htmlFor='password'>Contraseña</label>
              <input
                type='password'
                className='form-control'
                name='password'
                value={password}
                onChange={onChange}
                required
              />
            </div>
            <div className='form-group'>
              <label htmlFor='password2'>Repetir contraseña</label>
              <input
                type='password'
                className='form-control'
                name='password2'
                value={password2}
                onChange={onChange}
                required
              />
            </div>
            <button type='submit' className='btn btn-primary'>
              Registrarse
            </button>
          </form>
        </div>
      </div>
    </RegisterWrapper>
  )
}

Register.propTypes = {
  register: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
})

export default connect(mapStateToProps, { register })(Register)
