import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

// Redux
import { connect } from 'react-redux'

const AlertsWrapper = styled.div`
  text-align: center;
`

const Alerts = ({ alerts }) => {
  return (
    alerts !== null &&
    alerts.length > 0 && (
      <AlertsWrapper>
        {alerts.map((alert) => (
          <div
            key={alert.id}
            className={`alert alert-${alert.alertType}`}
            role='alert'
          >
            {alert.msg}
          </div>
        ))}
      </AlertsWrapper>
    )
  )
}

Alerts.propTypes = {
  alerts: PropTypes.array.isRequired,
}

const mapStateToProps = (state) => ({
  alerts: state.alerts,
})

export default connect(mapStateToProps)(Alerts)
