import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

// Redux
import { connect } from 'react-redux'

// Components
import Incomes from './incomes/Incomes'
import Outcomes from './outcomes/Outcomes'
import Preloader from './Preloader'

const DashboardWrapper = styled.div`
  height: calc(100vh - 91px);
`

const Dashboard = ({ auth: { user, loading } }) => {
  return loading || user === null ? (
    <Preloader />
  ) : (
    <DashboardWrapper>
      <div className='d-flex justify-content-between h-100 py-3'>
        <div className='w-50 h-100 d-flex flex-column mr-2'>
          <h3 className='card-title'>Ingresos</h3>
          <div className='h-100 mb-3 overflow-auto border-bottom'>
            <Incomes />
          </div>
          <Link className='btn btn-primary' to='/new-income'>
            Nuevo ingreso
          </Link>
        </div>

        <div className='w-50 h-100 d-flex flex-column ml-2'>
          <h3 className='card-title'>Egresos</h3>
          <div className='h-100 mb-3 overflow-auto border-bottom'>
            <Outcomes />
          </div>
          <Link className='btn btn-primary' to='/new-outcome'>
            Nuevo egreso
          </Link>
        </div>
      </div>
    </DashboardWrapper>
  )
}

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
})

export default connect(mapStateToProps)(Dashboard)
