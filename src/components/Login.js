import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import styled from 'styled-components'

// Redux
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Actions
import { login } from '../actions/authActions'

const LoginWrapper = styled.div`
  max-width: 480px;
`

const Login = ({ login, auth: { isAuthenticated, loading } }) => {
  const [formData, setFormData] = useState({
    username: '',
    password: '',
  })

  const { username, password } = formData

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value })

  const onSubmit = (e) => {
    e.preventDefault()
    login({ user: username, pass: password })
  }

  // Redirect if logged in
  if (isAuthenticated) {
    return <Redirect to='/dashboard' />
  }

  return (
    <LoginWrapper className='m-auto py-3'>
      <div className='card'>
        <div className='card-body'>
          <h3 className='card-title text-center'>Ingresar</h3>
          <form onSubmit={onSubmit}>
            <div className='form-group'>
              <label htmlFor='username'>Usuario</label>
              <input
                type='text'
                className='form-control'
                name='username'
                value={username}
                onChange={onChange}
                required
              />
            </div>
            <div className='form-group'>
              <label htmlFor='password'>Contraseña</label>
              <input
                type='password'
                className='form-control'
                name='password'
                value={password}
                onChange={onChange}
                required
              />
            </div>
            <button
              type='submit'
              className='btn btn-primary w-100'
              disabled={loading}
            >
              {loading ? 'Iniciando sesión...' : 'Ingresar'}
            </button>
          </form>
        </div>
      </div>
    </LoginWrapper>
  )
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
})

export default connect(mapStateToProps, { login })(Login)
