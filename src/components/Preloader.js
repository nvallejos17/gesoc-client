import React from 'react'
import styled from 'styled-components'

const PreloaderWrapper = styled.div`
  width: 100%;
  height: 100%;

  position: fixed;
  top: 0;
  left: 0;
  z-index: 5;

  display: flex;
  justify-content: center;
  align-items: center;
`

const Preloader = () => (
  <PreloaderWrapper>
    <div className='spinner-border' />
  </PreloaderWrapper>
)

export default Preloader
