import { SET_CATEGORIES, GET_CATEGORIES, CATEGORIES_ERROR } from '../types'

const initialState = []

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case SET_CATEGORIES:
    case GET_CATEGORIES:
      return payload

    case CATEGORIES_ERROR:
      console.error(
        'CATEGORIES_ERROR: Hubo un error al procesar la información'
      )
      return state

    default:
      return state
  }
}
