import {
  SET_OUTCOMES,
  GET_OUTCOMES,
  ADD_OUTCOME,
  OUTCOMES_ERROR,
} from '../types'

const initialState = []

export default (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case SET_OUTCOMES:
    case GET_OUTCOMES:
      return payload

    case ADD_OUTCOME:
      return [payload, ...state]

    case OUTCOMES_ERROR:
      console.error('OUTCOMES_ERROR: Hubo un error al procesar la información')
      return state

    default:
      return state
  }
}
