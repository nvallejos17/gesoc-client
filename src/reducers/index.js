import { combineReducers } from 'redux'
import authReducer from './authReducer'
import incomesReducer from './incomesReducer'
import outcomesReducer from './outcomesReducer'
import alertsReducer from './alertsReducer'
import categoriesReducer from './categoriesReducer'

export default combineReducers({
  auth: authReducer,
  incomes: incomesReducer,
  outcomes: outcomesReducer,
  alerts: alertsReducer,
  categories: categoriesReducer,
})
