import { SET_INCOMES, GET_INCOMES, ADD_INCOME, INCOMES_ERROR } from '../types'

const initialState = []

export default (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case SET_INCOMES:
    case GET_INCOMES:
      return payload

    case ADD_INCOME:
      return [payload, ...state]

    case INCOMES_ERROR:
      console.error('INCOMES_ERROR: Hubo un error al procesar la información')
      return state

    default:
      return state
  }
}
