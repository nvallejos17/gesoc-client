FROM node

WORKDIR /usr/src/app
COPY . /usr/src/app

# Install dependencies
RUN npm install

# Start server
CMD [ "npm", "start" ]